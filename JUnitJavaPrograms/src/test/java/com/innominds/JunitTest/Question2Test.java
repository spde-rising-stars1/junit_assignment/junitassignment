package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import com.innominds.Junit.Question2;

public class Question2Test {

	@Test
	void ouestion2test() {

		List<Character> upper = new ArrayList<Character>();
		Collections.addAll(upper, 'G', 'K', 'O', 'E', 'E');
		assertEquals(upper, Question2.upperCharacter());
		List<Character> lower = new ArrayList<Character>();
		Collections.addAll(lower, 'e', 'e', 's', 'f', 'r', 'g', 'k', 's');
		assertEquals(lower, Question2.lowerCharacter());
		List<Character> number = new ArrayList<Character>();
		Collections.addAll(number, '0', '1', '0', '7');
		assertEquals(number, Question2.numberCharacter());
		List<Character> special = new ArrayList<Character>();
		Collections.addAll(special, '#', '@');
		assertEquals(special, Question2.specialCharacter());

	}
}
