package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;

import org.junit.jupiter.api.Test;
import com.innominds.Junit.Question7;

public class Question7Test {

	@Test
	void differenceBetDatesTest() {
		long number = anyLong();
		assertEquals(number, Question7.differenceBetweenDates());
	}
}
