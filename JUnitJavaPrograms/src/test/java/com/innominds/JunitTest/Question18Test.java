package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;

import org.junit.jupiter.api.Test;

import com.innominds.Junit.Question18;

public class Question18Test {

	@Test
	void ClockTest()
	{
		int hours=anyInt();
		int minutes=anyInt();
		int seconds=anyInt();
		assertEquals(hours, minutes, seconds,Question18.displayClock());
	}
}
