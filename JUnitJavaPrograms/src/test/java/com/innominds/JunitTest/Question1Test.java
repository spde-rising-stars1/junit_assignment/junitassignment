package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.innominds.Junit.Question1;

class Question1Test {

	@Test
	void TestNonStaticVariable() {

		Question1 question1 = new Question1();
		Question1 question2 = new Question1();
		assertNotEquals(2, question2.getNonStaticVariable());

	}

	@Test
	void TestStaticVariable() {
		Question1 question3 = new Question1();
		Question1 question4 = new Question1();
		assertEquals(2, Question1.getStaticVariable());
	}
}
