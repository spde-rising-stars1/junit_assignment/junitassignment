package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import com.innominds.Junit.Question23;


public class Question23Test {

	@Test
	void EmployeeSortingTest()
	{
		ArrayList<Question23> employee = new ArrayList<>();
		employee.add(new Question23(1, "Adesh", "SPDE"));
		employee.add(new Question23(3, "Amit", "SPDE"));
		employee.add(new Question23(2, "Saurabh", "SPDE"));
		assertEquals(employee,Question23.Sorting());
		
	}
}
