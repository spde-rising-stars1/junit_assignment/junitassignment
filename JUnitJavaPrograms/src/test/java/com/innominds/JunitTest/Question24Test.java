package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

import com.innominds.Junit.Question24InvalidArraySize;
import com.innominds.Junit.Question24;

public class Question24Test {

	@Test
	void ArrayTest() throws Question24InvalidArraySize {
		int arr[] = { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100 };
		// assertTrue(Arrays.equals(arr, Question25.square()));
		assertArrayEquals(arr, Question24.square());

	}
}
