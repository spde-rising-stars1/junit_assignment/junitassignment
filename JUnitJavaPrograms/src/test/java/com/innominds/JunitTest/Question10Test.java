package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.innominds.Junit.Question10;

import org.junit.jupiter.api.Test;

public class Question10Test {

	@Test
	void duplicateCharacterTest()
	{
		List<Character> character = new ArrayList<Character>();
		Collections.addAll(character, 'b','d');
		assertEquals(character,Question10.duplicateCharacter());
	}
}
