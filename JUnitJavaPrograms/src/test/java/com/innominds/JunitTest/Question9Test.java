package com.innominds.JunitTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;

import org.junit.jupiter.api.Test;
import com.innominds.Junit.Question9;

public class Question9Test {

	@Test
	void IncomeTaxTest()
	{
		int number = anyInt();
		assertEquals(number, Question9.IncomeTax(number));
	}
}
