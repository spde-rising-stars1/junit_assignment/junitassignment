package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyDouble;


import org.junit.jupiter.api.Test;

import com.innominds.Junit.Question19;

public class Question19Test {

	@Test
	void diameterTest()
	{
		double myX=anyDouble();
		double myY=anyDouble();
		double myDiameter=anyDouble();
		assertEquals(myX,myY,myDiameter,Question19.calculateDiameter());
	}
}
