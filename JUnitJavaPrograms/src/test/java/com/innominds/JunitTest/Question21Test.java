package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import java.util.Collections;
import java.util.LinkedList;
import org.junit.jupiter.api.Test;
import com.innominds.Junit.Question6;

public class Question21Test {

	@Test
	void Ouestion6Test() {
		LinkedList<Integer> list1 = new LinkedList<Integer>();
		Collections.addAll(list1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20);
		assertEquals(list1, Question6.saveEvenNumbers(20));
		LinkedList<Integer> list2 = new LinkedList<Integer>();
		Collections.addAll(list2, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40);
		assertEquals(list2, Question6.printEvenNumbers());
		int number = anyInt();
		assertEquals(number, Question6.printEvenNumbers(18));
	}
}
