package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.innominds.Junit.Question8;

public class Question8Test {

	@Test
	void EvenNumbersTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		Collections.addAll(numbers, 2, 4, 6, 8, 10);
		assertEquals(numbers, Question8.findEvenNumber());
	}
}
