package com.innominds.JunitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import com.innominds.Junit.Question14Employee;
import com.innominds.Junit.Question14EmployeeDB;

public class Question14EmployeeTest {

	@Test
	void addEmployeeTest() {
		Question14Employee newemployee = new Question14Employee(1, "Adesh Kesarkar", "adesh@test.com", 'M', 40000);
		boolean employee = true;
		assertEquals(employee, Question14EmployeeDB.addEmployee(newemployee));
	}

	@Test
	void showPaySlipTest() {
		int payslip = anyInt();
		int id = anyInt();
		assertEquals(id, payslip, Question14EmployeeDB.showPaySlip(3));
	}

	@Test
	void deleteEmployeeTest() {
		boolean employee = anyBoolean();
		assertEquals(employee, Question14EmployeeDB.deleteEmployee(2));
	}

	// not working
	@Test
	void showAllEmployeeTest() {
		ArrayList<Question14Employee> employee = new ArrayList<>();
		employee.add(new Question14Employee(1, "Adesh Kesarkar", "adesh@test.com", 'M', 40000));
		employee.add(new Question14Employee(2, "Vaibhav Sahu", "vaibhav@test.com", 'M', 45000));
		employee.add(new Question14Employee(3, "Kunal Asnani", "kunal@test.com", 'M', 50000));
		employee.add(new Question14Employee(4, "Amit Anwade", "amit@test.com", 'M', 55000));
		assertEquals(employee, Question14EmployeeDB.listAll());

	}

}
