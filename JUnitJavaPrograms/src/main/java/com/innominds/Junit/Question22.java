package com.innominds.Junit;

public class Question22 {

	interface Add {
		void getSum(int a, int b);
	}

	public static void main(String[] args) {

		Add add = (x, y) -> System.out.println("Addition of two numbers is : " + (x + y));
		add.getSum(5, 5);

	}

}
