package com.innominds.Junit;

@SuppressWarnings("serial")
public class Question19ExplicitValueException extends Exception {

	public Question19ExplicitValueException(String str) {
		super(str);
	}
}
