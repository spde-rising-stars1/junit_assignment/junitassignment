package com.innominds.Junit;

import java.util.LinkedList;

public class Question21 {
	private static LinkedList<Integer> A1 = new LinkedList<Integer>();

	public static LinkedList<Integer> saveEvenNumbers(int N) {
		A1 = new LinkedList<Integer>();

		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0)
				A1.add(i);
		}
		System.out.println("Elements of List are A1 " + A1);
		return A1;
	}

	public static LinkedList<Integer> printEvenNumbers() {
		LinkedList<Integer> A2 = new LinkedList<Integer>();
		for (int list : A1) {
			A2.add(list * 2);
		}
		System.out.println("Elements of List are A2 " + A2);
		return A2;
	}

	public static int printEvenNumbers(int N) {
		for (int list : A1) {
			if (list == N) {
				System.out.println("The number " + list + " is present is the list. ");
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		Question6.saveEvenNumbers(20);
		Question6.printEvenNumbers();
		Question6.printEvenNumbers(18);

	}
}
