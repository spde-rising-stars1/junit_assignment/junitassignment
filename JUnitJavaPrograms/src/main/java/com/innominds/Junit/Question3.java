package com.innominds.Junit;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Question3 {
	public static void main(String[] args) throws IOException {
		
		String text = "My Name is Saurabh \nMy Nickname is Superman ";

		Path path1 = Path.of("D://InnomindsTask/demo1.txt");
		Path path2 = Path.of("D://InnomindsTask/demo2.txt");
		Files.writeString(path1, text);
		Files.writeString(path2, text);
		String contentofdemo1 = Files.readString(path1);
		String contentofdemo2 = Files.readString(path2);
		System.out.println(contentofdemo1);
		System.out.println(contentofdemo2);
	}
}