package com.innominds.Junit;

import java.util.Scanner;

public class Question9 {

	public static int IncomeTax(int slab) {
		Scanner sc = new Scanner(System.in);
		System.out.println("The Slab rates are : ");
		System.out.println("1.Upto 50000");
		System.out.println("2.Upto 60000");
		System.out.println("3.Upto 150000");
		System.out.println("4.Above 150000");
		System.out.println("Enter your Slab rate amount : ");

		if (slab <= 50000) {
			System.out.println("Your Income Tax is : 0 ");
		} else if (slab > 50000 && slab <= 60000) {
			System.out.println("Your Income Tax is : 1000 ");
		} else if (slab > 60000 && slab <= 150000) {
			System.out.println("Your Income Tax is : 18000 ");
		} else if (slab >= 150000) {
			System.out.println("Your Income Tax is : 27000 ");
		}
		sc.close();
		return slab;
	}

	public static void main(String[] args) {

		IncomeTax(50000);
	}

}
