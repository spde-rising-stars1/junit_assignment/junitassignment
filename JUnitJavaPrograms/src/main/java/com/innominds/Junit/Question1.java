package com.innominds.Junit;

public class Question1 {
	private static int staticVariable;
	private int nonStaticVariable = 0;

	static {
		staticVariable = 0;
	}

	public Question1() {
		nonStaticVariable++;
		staticVariable++;
	}

	public int getNonStaticVariable() {
		return nonStaticVariable;
	}

	public static int getStaticVariable() {
		return staticVariable;
	}

	public static void main(String[] args) {

		Question1 question1 = new Question1();
		Question1 question2 = new Question1();

		System.out.println(question2.getNonStaticVariable());

		Question1 question3 = new Question1();
		Question1 question4 = new Question1();

		System.out.println(Question1.getStaticVariable());
	}

}
