package com.innominds.Junit;

public class Question19 {
	private static double myX;
	private static double myY;
	private static double myDiameter;

	public Question19() {
		super();
	}

	public Question19(double myX, double myY, double myDiameter) throws Question19ExplicitValueException {
		
		double diameter= 2 * (Math.sqrt((Math.pow(myX, 2) + Math.pow(myY, 2))));
		if(myDiameter != diameter )
		{
			throw new Question19ExplicitValueException("explicit values of "+diameter+" and "+myDiameter+ " are invalid. ");
		}
		Question19.myX=myX;
		Question19.myY=myY;
		Question19.myDiameter = myDiameter;
	}

	public static String calculateDiameter() {
		String diameter = "The Diameter of Circle is : " + myDiameter;
		return diameter;
	}

	public static void main(String[] args) throws Question19ExplicitValueException {

		new Question19(3, 3, 8.48528137423857);
		Question19.calculateDiameter();
		System.out.println("The Diameter of Circle is : " + myDiameter);

	}

}
