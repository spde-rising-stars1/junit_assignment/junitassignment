package com.innominds.Junit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Question2 {

	public static List<Character> upperCharacter() {
		String str = "#GeeKs01fOr@gEEks07";
		List<Character> upper = new ArrayList<>();
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch >= 'A' && ch <= 'Z') {
				upper.add(ch);
			}
		}
		System.out.println(upper);
		return upper.stream().collect(Collectors.toList());
	}

	public static List<Character> lowerCharacter() {
		String str = "#GeeKs01fOr@gEEks07";
		List<Character> lower = new ArrayList<>();
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch >= 'a' && ch <= 'z') {
				lower.add(ch);
			}
		}
		System.out.println(lower);
		return lower.stream().collect(Collectors.toList());
	}

	public static List<Character> numberCharacter() {
		String str = "#GeeKs01fOr@gEEks07";
		List<Character> number = new ArrayList<>();
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch >= '0' && ch <= '9') {
				number.add(ch);
			}
		}
		System.out.println(number);
		return number.stream().collect(Collectors.toList());
	}

	public static List<Character> specialCharacter() {
		String str = "#GeeKs01fOr@gEEks07";
		List<Character> special = new ArrayList<>();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetterOrDigit(c)) {
				special.add(c);
			}
		}
		System.out.println(special);
		return special.stream().collect(Collectors.toList());
	}

	public static void main(String args[]) {
		upperCharacter();
		lowerCharacter();
		numberCharacter();
		specialCharacter();
	}
}
