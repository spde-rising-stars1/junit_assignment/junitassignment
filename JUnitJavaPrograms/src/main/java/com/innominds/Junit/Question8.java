package com.innominds.Junit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class Question8 {

	public static List<Integer> findEvenNumber() {
		List<Integer> numbers = new ArrayList<Integer>();
		Collections.addAll(numbers, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		System.out.println("The Even Numbers are : ");
		return numbers.stream().filter(num -> num % 2 == 0).collect(Collectors.toList());
	}

	public static void main(String[] args) {
		List<Integer> findEvenNumber = findEvenNumber();
		System.out.println(findEvenNumber);
	}
}

