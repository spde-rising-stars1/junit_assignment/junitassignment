package com.innominds.Junit;

@SuppressWarnings({ "resource", "serial" })
public  class Question24InvalidArraySize extends Exception {
	public Question24InvalidArraySize(String str) {
		super(str);
	}
}
