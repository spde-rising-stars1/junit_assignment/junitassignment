package com.innominds.Junit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Question14EmployeeDB {
	static List<Question14Employee> employeedbarraylist = new ArrayList<>();

	public static boolean addEmployee(Question14Employee e) {
		return employeedbarraylist.add(e);
	}

	public static boolean deleteEmployee(int empId) {
		boolean removed = false;
		Iterator<Question14Employee> iterator = employeedbarraylist.iterator();

		while (iterator.hasNext()) {
			Question14Employee employee = iterator.next();
			if (employee.getEmpId() == empId) {
				removed = true;
				iterator.remove();
			}
		}
		return removed;
	}

	public static String showPaySlip(int empId) {
		String payslip = "Invalid employeeId";

		for (Question14Employee e : employeedbarraylist) {
			if (e.getEmpId() == empId) {
				payslip = "Pay slip for employeeId " + empId + " is " + e.getEmpSalary();
			}
		}

		return payslip;
	}

	public static Question14Employee[] listAll() {
		Question14Employee[] employeearray = new Question14Employee[employeedbarraylist.size()];
		for (int i = 0; i < employeedbarraylist.size(); i++)
			employeearray[i] = employeedbarraylist.get(i);
		return employeearray;
	}

}