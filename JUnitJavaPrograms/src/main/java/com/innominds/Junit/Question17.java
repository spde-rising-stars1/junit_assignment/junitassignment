package com.innominds.Junit;

import java.util.Scanner;

public class Question17 {

	public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
		System.out.println("Enter maximum number : ");
		int maxnumber = sc.nextInt();
		System.out.println("List of the prime number from 1 to " + maxnumber + " is ");
		for (int number = 2; number <= maxnumber; number++) {
			boolean isPrime = true;
			for (int i = 2; i <= number / 2; i++) {
				if (number % i == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime == true)
				System.out.println(number);
			sc.close();
		}
	}
}
