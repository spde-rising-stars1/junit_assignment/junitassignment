package com.innominds.Junit;

public class Question14EmployeeMain {

	public static void main(String[] args) {

		Question14Employee employee1 = new Question14Employee(1, "Adesh Kesarkar", "adesh@test.com", 'M', 40000);
		Question14Employee employee2 = new Question14Employee(2, "Vaibhav Sahu", "vaibhav@test.com", 'M', 45000);
		Question14Employee employee3 = new Question14Employee(3, "Kunal Asnani", "kunal@test.com", 'M', 50000);
		Question14Employee employee4 = new Question14Employee(4, "Amit Anwade", "amit@test.com", 'M', 55000);

		Question14EmployeeDB.addEmployee(employee1);
		Question14EmployeeDB.addEmployee(employee2);
		Question14EmployeeDB.addEmployee(employee3);
		Question14EmployeeDB.addEmployee(employee4);

		for (Question14Employee employee : Question14EmployeeDB.listAll())
			System.out.println(employee.GetEmployeeDetails());

		System.out.println();
		Question14EmployeeDB.deleteEmployee(2);

		for (Question14Employee employee : Question14EmployeeDB.listAll())
			System.out.println(employee.GetEmployeeDetails());

		System.out.println();

		System.out.println(Question14EmployeeDB.showPaySlip(3));
	}

}
