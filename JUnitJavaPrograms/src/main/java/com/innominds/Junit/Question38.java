package com.innominds.Junit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Question38 {

	public static String datetime1() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss  z");
		Date date1 = new Date();
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		System.out.println(format.format(date1));
	    return (format.format(date1));
	}
	
	public static String datetime2() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss  z");
		Date date1 = new Date();
		Date date2 = new Date(date1.getTime() + 10 * 60 * 60 * 1000);
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		System.out.println(format.format(date2));
	    return (format.format(date2));
	}

	public static void main(String[] args) {
		datetime1();
		datetime2();
	}
}
