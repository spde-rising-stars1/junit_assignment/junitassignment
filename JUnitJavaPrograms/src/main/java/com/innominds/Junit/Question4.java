package com.innominds.Junit;

public class Question4 extends Thread {
	
	public Question4(String name) {
		super(name);
	}

	public void run() {
		if (Thread.currentThread().isDaemon()) {
			System.out.println(getName() + " is Daemon thread");
		}

		else {
			System.out.println(getName() + " is User thread");
		}
	}

	public static void main(String[] args) {

		Question4 t1 = new Question4("t1");
		Question4 t2 = new Question4("t2");
		Question4 t3 = new Question4("t3");
		t1.setDaemon(true);
		t1.start();
		t2.start();
		t3.setDaemon(true);
		t3.start();
	}
}
