package com.innominds.Junit;

import java.util.ArrayList;
import java.util.List;

public class Question10 {

	public static List<Character> duplicateCharacter() {
		List<Character> character = new ArrayList<Character>();
		String str = "abcdebd";
		System.out.println("The string is:" + str);
		System.out.print("Duplicate Characters in above string are: ");
		for (int i = 0; i < str.length(); i++) {
			for (int j = i + 1; j < str.length(); j++) {
				if (str.charAt(i) == str.charAt(j)) {
					character.add(str.charAt(j));
					break;
				}
			}
		}

		return character;

	}

	public static void main(String[] args) {
		List<Character> character = duplicateCharacter();
		System.out.println(character);
	}
}
