package com.innominds.Junit;

public class Question18 {
	private static int hours;
	private static int minutes;
	private static int seconds;
	private static String mode;

	public Question18() {
		super();
	}

	public Question18(int hour, int minute, int second ) {
		if (second >= 60) {
			minute = minute + (second / 60);
			second = second % 60;
		}
		if (minute >= 60) {
			hour = hour + (minute / 60);
			minute = minute % 60;
		}
		if (hour <= 12 || (hour >= 24 && hour <= 36)){
			hour = hour % 24;
			mode = "AM";
		}
		else if (hour >= 12) {
			hour = hour % 12;
			mode = "PM";
		}
		
		Question18.hours = hour;
		Question18.minutes = minute;
		Question18.seconds = second;
	}

	public static String displayClock() {
		String time = "The current time in hour : minute : second with phase is " + hours + ":" + minutes + ":" + seconds + " " + mode ;
		return time;
	}

	public static void main(String[] args) {
		new Question18(16, 20, 180);
		Question18.displayClock();
		System.out.println("The current time in hour : minute : second with phase is " + hours + ":" + minutes + ":" + seconds + " " + mode);

	}
}
