package com.innominds.Junit;

class Thread1 implements Runnable {

	public void run() {
		for (int i = 0; i < 10; i++) {

			System.out.println("Good Morning ");
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}
	}
}

class Thread2 implements Runnable {

	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Hello ");
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
			}
		}
	}
}

class Thread3 implements Runnable {

	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Welcome ");
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
			}
		}
	}
}

public class Question34 {
	public static void main(String[] args) {
		Runnable runable1 = new Thread1();
		Runnable runable2 = new Thread2();
		Runnable runable3 = new Thread3();
		Thread thread1 = new Thread(runable1);
		Thread thread2 = new Thread(runable2);
		Thread thread3 = new Thread(runable3);

		thread1.start();
		thread2.start();
		thread3.start();
	}
}
