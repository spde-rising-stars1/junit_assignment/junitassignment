package com.innominds.Junit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Question7 {

	public static long differenceBetweenDates() {

		Scanner sc = new Scanner(System.in);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			System.out.println("Enter 1st Date in dd/MM/yyyy format : ");
			String Beforedate = sc.next();
			System.out.println("Enter 2nd Date in dd/MM/yyyy format : ");
			String Afterdate = sc.next();
			Date firstdate = sdf.parse(Beforedate);
			Date seconddate = sdf.parse(Afterdate);

			long TimeDifference = Math.abs(seconddate.getTime() - firstdate.getTime());
			long DaysDifference = TimeUnit.DAYS.convert(TimeDifference, TimeUnit.MILLISECONDS);
			System.out.println("The number of days between two dates is : " + DaysDifference);
		} catch (Exception e) {
			e.printStackTrace();
		}
        sc.close();
		return 0;
	}

	public static void main(String[] args) {

		differenceBetweenDates();

	}

}
