package com.innominds.Junit;

import java.util.Scanner;

public class Question24 {

	@SuppressWarnings("resource")
	public static int[] square() throws Question24InvalidArraySize {
		int numbers[] = new int[10];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number upto which you want to find squares of numbers : ");
		int num = sc.nextInt();
		if (num <= 11) {
			for (int i = 0; i < num; i++) {
				int j = i + 1;
				numbers[i] = j * j;
			}
		} else {
			throw new Question24InvalidArraySize("Array size is more than 10");
		}
		for (int value : numbers) {
			System.out.println(value);
		}

		sc.close();

		return numbers;

	}

	public static void main(String[] args) {
		try {
			square();
		} catch (Question24InvalidArraySize e) {
			e.printStackTrace();
		}
	}

}
