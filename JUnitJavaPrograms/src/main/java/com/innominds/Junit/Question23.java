package com.innominds.Junit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Question23 {
	private int id;
	private String name;
	private String department;

	public Question23() {
		super();
	}

	public Question23(int id, String name, String department) {
		super();
		this.id = id;
		this.name = name;
		this.department = department;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	

	@Override
	public int hashCode() {
		return Objects.hash(department, id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question23 other = (Question23) obj;
		return Objects.equals(department, other.department) && id == other.id && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "employee [id=" + id + ", name=" + name + ", department=" + department + "]";
	}

	public static List<Question23> Sorting() {
		ArrayList<Question23> employee = new ArrayList<>();
		employee.add(new Question23(1, "Adesh", "SPDE"));
		employee.add(new Question23(3, "Amit", "SPDE"));
		employee.add(new Question23(2, "Saurabh", "SPDE"));
		System.out.println("All Cricketer Are Sorted by Name in Asending Order : ");
		return employee.stream().sorted((Question23 s1, Question23 s2) -> s1.getName().compareTo(s2.getName()))
				.collect(Collectors.toList());

	}

	public static void main(String[] args) {
		List<Question23> sorting = Sorting();
		System.out.println(sorting);
	}

}
