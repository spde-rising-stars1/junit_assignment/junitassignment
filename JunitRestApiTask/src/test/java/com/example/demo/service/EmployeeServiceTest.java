package com.example.demo.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.dao.EmployeeDao;
import com.example.demo.entity.Employee;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

	@Mock
	private EmployeeDao dao;

	@InjectMocks
	private EmployeeService service;

	@Test
	public void getAllEmployeeTest() {
		List<Employee> employeelist = new ArrayList<>();
		employeelist.add(new Employee(1, "Adesh Kesarkar", "adesh@test.com", 'M', 50000));
		employeelist.add(new Employee(2, "Amit Anwade", "amit@test.com", 'M', 40000));
		employeelist.add(new Employee(3, "Syed Ahemd", "syed@test.com", 'M', 30000));

		when(dao.findAll()).thenReturn(employeelist);
		assertEquals(3, service.getAllEmployee().size());
	}

	@Test
	public void saveNewEmployeeTest() {
		Employee employee = new Employee(1, "Vaibhav Sahu", "vaibhav@test.com", 'M', 60000);
		when(dao.save(employee)).thenReturn(employee);
		assertEquals(employee, service.addNewEmployee(employee));

	}
	

	@Test
	public void updateEmployeeTest()
	{
		Employee employee = new Employee(1, "Amit Anwade", "amit@test.com", 'M', 50000);
		employee.setEmpSalary(60000);
		when(dao.findById(1)).thenReturn(employee);
		when(dao.save(employee)).thenReturn(employee);
		assertEquals(employee, service.updateEmployee(employee));	
	}
	
	
	@Test
	public void deleteEmployeeTest()
	{
		Employee employee = new Employee(1, "Kunal Asnani", "kunal@test.com", 'M', 50000);
		when(dao.existsById(1)).thenReturn(true);
		service.deleteEmployee(1);
		verify(dao, times(1)).deleteById(1);
	}
	
	
}
