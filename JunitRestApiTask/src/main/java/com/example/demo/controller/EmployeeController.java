package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService empservice;

	@GetMapping("/employee/all")
	public List<Employee> getAllEmployee() {
		List<Employee> employee = empservice.getAllEmployee();
		return employee;
	}

	@PostMapping("/employee/add")
	public Employee addNewEmployee(@RequestBody Employee emp) {
		Employee employee = empservice.addNewEmployee(emp);
		return employee;
	}

	@PutMapping("/employee/update")
	public Employee updateEmployee(@RequestBody Employee emp) {
		Employee employee = empservice.updateEmployee(emp);
		return employee;
	}

	@DeleteMapping("/employee/delete/{id}")
	public int deleteEmployee(@PathVariable("id") int id) {
		int employee = empservice.deleteEmployee(id);
		return employee;
	}
}
