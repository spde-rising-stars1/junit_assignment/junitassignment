package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EmployeeDao;
import com.example.demo.entity.Employee;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	private EmployeeDao empdao;

	public List<Employee> getAllEmployee() {
		List<Employee> emp = empdao.findAll();
		return emp;
	}

	public Employee addNewEmployee(Employee emp) {
		if (empdao.existsById(emp.getEmpId())) {
			return null;
		} else {
			Employee employee = empdao.save(emp);
			return employee;
		}
	}

	public Employee updateEmployee(Employee emp) {

		Employee employee = empdao.findById(emp.getEmpId());
		if (employee != null) {
			employee.setEmpSalary(emp.getEmpSalary());
			empdao.save(employee);
			return employee;
		}
		return null;
	}

	public int deleteEmployee(int id) {
		if (empdao.existsById(id)) {
			empdao.deleteById(id);

		} else {
			return 0;
		}
		return id;
	}

}
