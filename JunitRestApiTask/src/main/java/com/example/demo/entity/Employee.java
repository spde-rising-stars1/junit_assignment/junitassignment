package com.example.demo.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {
//EmpId | EmpName        | EmpEmail       | EmpGender | EmpSalary

	@Id
	@Column
	private int EmpId;
	@Column
	private String EmpName;
	@Column
	private String EmpEmail;
	@Column
	private char EmpGender;
	@Column
	private float EmpSalary;

	public Employee() {
		super();
	}

	public Employee(int empId, String empName, String empEmail, char empGender, float empSalary) {
		super();
		this.EmpId = empId;
		this.EmpName = empName;
		this.EmpEmail = empEmail;
		this.EmpGender = empGender;
		this.EmpSalary = empSalary;
	}

	public Employee(int empId, float empSalary) {
		super();
		EmpId = empId;
		EmpSalary = empSalary;
	}

	public int getEmpId() {
		return EmpId;
	}

	public void setEmpId(int empId) {
		this.EmpId = empId;
	}

	public String getEmpName() {
		return EmpName;
	}

	public void setEmpName(String empName) {
		this.EmpName = empName;
	}

	public String getEmpEmail() {
		return EmpEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.EmpEmail = empEmail;
	}

	public char getEmpGender() {
		return EmpGender;
	}

	public void setEmpGender(char empGender) {
		this.EmpGender = empGender;
	}

	public float getEmpSalary() {
		return EmpSalary;
	}

	public void setEmpSalary(float empSalary) {
		this.EmpSalary = empSalary;
	}

	@Override
	public String toString() {
		return "Employee [EmpId=" + EmpId + ", EmpName=" + EmpName + ", EmpEmail=" + EmpEmail + ", EmpGender="
				+ EmpGender + ", EmpSalary=" + EmpSalary + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(EmpEmail, EmpGender, EmpId, EmpName, EmpSalary);
	}

}
